package com.aji.marketapi.api.v1.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@ApiModel(value = "Merchant", description = "merchant")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MerchantDTO {

    @ApiModelProperty(value = "Merchant Name", required = true, allowEmptyValue = false)
    @NotBlank
    @Size(min = 1, max = 255)
    private String MerchantName;

    @ApiModelProperty(value = "Only available in response", readOnly = true)
    private String MerchantSignature;

	public void setMerchantCrypto(String MerchantSignature2) {
		MerchantSignature = MerchantSignature2;
	}

	public Object getMerchantName() {
		return MerchantName;
	}


}
