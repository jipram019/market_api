package com.aji.marketapi.api.v1.mapper;

import com.aji.marketapi.api.v1.model.MerchantDTO;
import com.aji.marketapi.domain.Merchant;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface MerchantMapper {
    MerchantMapper INSTANCE = Mappers.getMapper(MerchantMapper.class);
    MerchantDTO MerchantToMerchantDTO(Merchant Merchant);
    Merchant MerchantDTOToMerchant(MerchantDTO MerchantDTO);
}
