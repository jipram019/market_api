package com.aji.marketapi.api.v1.model;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@ApiModel(value = "Merchant List", description = "Merchant list")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MerchantListDTO {
    public List<MerchantDTO> Merchants;
}
