package com.aji.marketapi.repositories;

import com.aji.marketapi.domain.Category;
import io.swagger.annotations.Api;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Category repository class for database interaction
 */
@Repository
@Api(tags = "Category Entity")
public interface CategoryRepository extends JpaRepository<Category, Long> {
    Category findByCategoryName(String categoryName);
}
