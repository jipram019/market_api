package com.aji.marketapi.repositories;

import com.aji.marketapi.domain.Merchant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Merchant repository class for database interaction
 */
@Repository
public interface MerchantRepository extends JpaRepository<Merchant, Long> {
}
