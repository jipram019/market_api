package com.aji.marketapi.repositories;

import com.aji.marketapi.domain.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Customer repository class for database interaction
 */
@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {
    Customer getCustomerByCustomerId(Long customerId);
}
