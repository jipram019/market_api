package com.aji.marketapi.services;

import com.aji.marketapi.api.v1.model.CategoryDTO;
import java.util.List;

/**
 * Category Interface
 */
public interface CategoryService {
    List<CategoryDTO> getCategories();
    CategoryDTO findByCategoryName(String categoryName);
}
