package com.aji.marketapi.services;

import com.aji.marketapi.api.v1.model.MerchantDTO;

import java.util.List;

/**
 * Merchant Interface
 */
public interface MerchantService {

    // Get all Merchants
    List<MerchantDTO> getMerchants();

    // Get Merchant by id
    MerchantDTO getMerchantById(Long MerchantId);

    // Create new Merchant
    MerchantDTO createMerchant(MerchantDTO MerchantDTO);

    // Put Merchant
    MerchantDTO updateMerchant(Long MerchantId, MerchantDTO MerchantDTO);

    // Patch customer
    MerchantDTO patchMerchant(Long MerchantId, MerchantDTO MerchantDTO);

    // Delete Merchant
    void deleteMerchantById(Long MerchantId);
}
