package com.aji.marketapi.services;

import com.aji.marketapi.api.v1.mapper.MerchantMapper;
import com.aji.marketapi.api.v1.model.MerchantDTO;
import com.aji.marketapi.controllers.v1.MerchantController;
import com.aji.marketapi.domain.Merchant;
import com.aji.marketapi.exceptions.ResourceNotFoundException;
import com.aji.marketapi.repositories.MerchantRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Merchant service implementation class
 */
@Service
public class MerchantServiceImpl implements MerchantService {
    /**
     * Injecting dependencies
     */
    private MerchantRepository MerchantRepository;
    private MerchantMapper MerchantMapper;

    /**
     * Constructor
     * @param MerchantRepository
     * @param MerchantMapper
     */
    public MerchantServiceImpl(MerchantRepository MerchantRepository, MerchantMapper MerchantMapper) {
        this.MerchantRepository = MerchantRepository;
        this.MerchantMapper = MerchantMapper;
    }

    /**
     * Method implementation for getting all Merchants from the database
     * @return
     */
    @Override
    public List<MerchantDTO> getMerchants() {
        return MerchantRepository.findAll()
                .stream()
                .map(Merchant -> {
                    MerchantDTO MerchantDTO = MerchantMapper.MerchantToMerchantDTO(Merchant);
                    MerchantDTO.setMerchantCrypto(getMerchantCrypto(Merchant.getMerchantId()));
                    return MerchantDTO;
                })
                .collect(Collectors.toList());
    }

    /**
     * Method implmentation for getting Merchant by provided Merchant Id
     * @param MerchantId
     * @return
     */
    @Override
    public MerchantDTO getMerchantById(Long MerchantId) {
        return  MerchantRepository.findById(MerchantId)
                .map(MerchantMapper::MerchantToMerchantDTO)
                .map(MerchantDTO -> {
                    MerchantDTO.setMerchantCrypto(getMerchantCrypto(MerchantId));
                    return MerchantDTO;
                })
                .orElseThrow(ResourceNotFoundException::new);
    }

    /**
     * Method implementation for creating a new Merchant by provided Merchant post data
     * @param MerchantDTO
     * @return
     */
    @Override
    public MerchantDTO createMerchant(MerchantDTO MerchantDTO) {
        Merchant Merchant = MerchantMapper.MerchantDTOToMerchant(MerchantDTO);
        return createOrUpdateMerchantHelper(Merchant);
    }

    /**
     * Helper method for create or update customer
     * @param Merchant
     * @return MerchantDTO
     */
    private MerchantDTO createOrUpdateMerchantHelper(Merchant Merchant) {
        Merchant saveMerchant = MerchantRepository.save(Merchant);
        MerchantDTO saveMerchantDTO = MerchantMapper.MerchantToMerchantDTO(saveMerchant);
        saveMerchantDTO.setMerchantCrypto(getMerchantCrypto(saveMerchant.getMerchantId()));
        return saveMerchantDTO;
    }

    /**
     * Method implementation for updating Merchant by Merchant Id
     * @param MerchantId
     * @param MerchantDTO
     * @return
     */
    @Override
    public MerchantDTO updateMerchant(Long MerchantId, MerchantDTO MerchantDTO) {
        return MerchantRepository.findById(MerchantId).map(Merchant -> {
            MerchantDTO MerchantDTO1 = null;
            if(Merchant.getMerchantId() != null) {
                Merchant saveMerchant = MerchantMapper.MerchantDTOToMerchant(MerchantDTO);
                saveMerchant.setMerchantId(MerchantId);
                MerchantDTO1 = createOrUpdateMerchantHelper(saveMerchant);
            }
            return MerchantDTO1;
        }).orElseThrow(ResourceNotFoundException::new);
    }

    /**
     * Method implmentation for partial updating Merchant by Merchant Id
     * @param MerchantId
     * @param MerchantDTO
     * @return
     */
    @Override
    public MerchantDTO patchMerchant(Long MerchantId, MerchantDTO MerchantDTO) {
        return MerchantRepository.findById(MerchantId).map(Merchant -> {
            if(MerchantDTO.getMerchantName() != null) {
                Merchant.setMerchantName(MerchantDTO.getMerchantName());
            }
            MerchantDTO saveMerchantDTO = MerchantMapper.MerchantToMerchantDTO(MerchantRepository.save(Merchant));
            saveMerchantDTO.setMerchantCrypto(getMerchantCrypto(MerchantId));
            return saveMerchantDTO;

        }).orElseThrow(ResourceNotFoundException::new);
    }

    /**
     * Method implementation for deleting Merchant by provided Merchant Id
     * @param MerchantId
     */
    @Override
    public void deleteMerchantById(Long MerchantId) {
        // Finding Merchant with the provided Merchant Id
        Optional<Merchant> Merchant = MerchantRepository.findById(MerchantId);

        // If Merchant found in database then do delete else
        // throw resource not found exception
        if(Merchant.isPresent()) {
            MerchantRepository.deleteById(MerchantId);
        } else throw new ResourceNotFoundException();
    }

    /**
     * Helper method for generating Merchant Crypto
     * @param MerchantId
     * @return
     */
    private String getMerchantCrypto(Long MerchantId) {
        return MerchantController.BASE_URL + "/" + MerchantId;
    }
}
