package com.aji.marketapi.controllers.v1;

import com.aji.marketapi.api.v1.model.MerchantDTO;
import com.aji.marketapi.api.v1.model.MerchantListDTO;
import com.aji.marketapi.services.MerchantService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * REST Controller for Merchant
 * GET POST PUT PATCH DELETE HTTP Methods are supported currently
 */
@Api(description = "Supports GET, POST, PATCH, PUT, DELETE operations", tags = {"Merchants"})
@RestController
@RequestMapping(MerchantController.BASE_URL)
public class MerchantController {
    public static final String BASE_URL = "/api/v1/Merchants";

    @Autowired
    private MerchantService MerchantService;
    
    public MerchantController(MerchantService MerchantService) {
        this.MerchantService = MerchantService;
    }

    @ApiOperation(value = "Lists all the Merchants", notes = "")
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public MerchantListDTO getMerchants() {
        return new MerchantListDTO();
    }

    @ApiOperation(value = "Get a Merchant by id", notes = "")
    @GetMapping(value = {"/{MerchantId}"}, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public MerchantDTO getMerchantById(@PathVariable Long MerchantId) {
        return MerchantService.getMerchantById(MerchantId);
    }

    @ApiOperation(value = "Create a Merchant", notes = "")
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Bad Request"),
    }
    )
	
    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public MerchantDTO createMerchant(@Validated @RequestBody MerchantDTO MerchantDTO) {
        return MerchantService.createMerchant(MerchantDTO);
    }

    @ApiOperation(value = "Replace a Merchant by new data", notes = "")
    @PutMapping(value = {"/{MerchantId}"}, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public MerchantDTO updateMerchant(@PathVariable Long MerchantId,@Validated @RequestBody MerchantDTO MerchantDTO) {
        return MerchantService.updateMerchant(MerchantId, MerchantDTO);
    }

    @ApiOperation(value = "Update a Merchant", notes = "")
    @PatchMapping(value = {"/{MerchantId}"}, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public MerchantDTO patchMerchant(@PathVariable Long MerchantId,@Validated @RequestBody MerchantDTO MerchantDTO) {
        return MerchantService.patchMerchant(MerchantId, MerchantDTO);
    }

    @ApiOperation(value = "Delete a Merchant", notes = "")
    @DeleteMapping(value = {"/{MerchantId}"}, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public void deleteMerchant(@PathVariable Long MerchantId) {
        MerchantService.deleteMerchantById(MerchantId);
    }
}
