package com.aji.marketapi.bootstrap;

import com.aji.marketapi.domain.Category;
import com.aji.marketapi.domain.Customer;
import com.aji.marketapi.domain.Merchant;
import com.aji.marketapi.repositories.CategoryRepository;
import com.aji.marketapi.repositories.CustomerRepository;
import com.aji.marketapi.repositories.MerchantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 * Bootstrap class to load default data in database on startup
 */
@Component
public class Bootstrap implements CommandLineRunner {

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private MerchantRepository MerchantRepository;

    public Bootstrap(CategoryRepository categoryRepository,
                     CustomerRepository customerRepository,
                     MerchantRepository MerchantRepository) {
        this.categoryRepository = categoryRepository;
        this.customerRepository = customerRepository;
        this.MerchantRepository   = MerchantRepository;
    }

    @Override
    public void run(String... args) throws Exception {
        // Load Categories
        addCategories();

        // Load Customers
        addCustomer();

        // Load Merchants
        addMerchants();
    }

    /**
     * Load categories in db
     */
    private void addCategories() {

        categoryRepository.deleteAll();

        Category foods = new Category();
        foods.setCategoryName("Foods");

        Category beverages = new Category();
        beverages.setCategoryName("Beverages");

        Category services = new Category();
        services.setCategoryName("Services");

        Category ride = new Category();
        ride.setCategoryName("Ride");

        Category payment = new Category();
        payment.setCategoryName("Payment");

        categoryRepository.save(foods);
        categoryRepository.save(beverages);
        categoryRepository.save(services);
        categoryRepository.save(ride);
        categoryRepository.save(payment);
    }

    /**
     * Load customers in db
     */
    private void addCustomer() {
        customerRepository.deleteAll();

        Customer one = new Customer();
        one.setFirstName("Aji");
        one.setLastName("Pramono");

        Customer two = new Customer();
        two.setFirstName("Berry");
        two.setLastName("Prima");

        Customer three = new Customer();
        three.setFirstName("Muhammad");
        three.setLastName("Ario");

        Customer four = new Customer();
        four.setFirstName("Aldi");
        four.setLastName("Taher");

        Customer five = new Customer();
        five.setFirstName("Maher");
        five.setLastName("Zain");

        customerRepository.save(one);
        customerRepository.save(two);
        customerRepository.save(three);
        customerRepository.save(four);
        customerRepository.save(five);
    }

    /**
     * Load categories in db
     */
    private void addMerchants() {
        MerchantRepository.deleteAll();

        Merchant MerchantOne = new Merchant();
        MerchantOne.setMerchantName("Western Tasty Fruits Ltd.");
        MerchantRepository.save(MerchantOne);

        Merchant MerchantTwo = new Merchant();
        MerchantTwo.setMerchantName("Fresh Juicy Company");
        MerchantRepository.save(MerchantTwo);

        Merchant MerchantThree = new Merchant();
        MerchantThree.setMerchantName("Yellow Bird Taxi Co.Ltd.");
        MerchantRepository.save(MerchantThree);

        Merchant MerchantFour = new Merchant();
        MerchantFour.setMerchantName("PT. Dompet Digital Nusantara");
        MerchantRepository.save(MerchantFour);

        Merchant MerchantFive = new Merchant();
        MerchantFive.setMerchantName("Cleanlite Laundry");
        MerchantRepository.save(MerchantFive);
    }
}
