package com.aji.marketapi.schedulers;

import com.aji.marketapi.repositories.CustomerRepository;
import com.aji.marketapi.repositories.MerchantRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class ScheduledTasks {

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private MerchantRepository merchantRepository;

    /**
     * Method for deleting all record from all the tables every 15 minutes
     */
    @Scheduled(fixedRate = 900000)
    public void databaseCleanUp() {
        customerRepository.deleteAll();
        merchantRepository.deleteAll();
    }
}
