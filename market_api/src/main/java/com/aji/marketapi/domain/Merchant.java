package com.aji.marketapi.domain;

import lombok.Data;
import javax.persistence.*;

/**
 * Merchant model class
 */
@Data
@Entity
public class Merchant {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long merchantId;

    @Column
    private String merchantName;

	public Long getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(Long merchantId2) {
		merchantId = merchantId2;
	}

	public void setMerchantName(Object merchantName2) {
		merchantName = (String)merchantName2;	
	}
}
