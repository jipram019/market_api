package com.aji.marketapi.domain;

import lombok.Data;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Customer model class
 */
@Data
@Entity
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long customerId;
    private String firstName;
    private String lastName;
	public Long getCustomerId() {
		return customerId;
	}
	public Long getCustomerById() {
		return customerId;
	}
	public void setCustomerId(Long customerId2) {
		customerId = customerId2;
	}
	public void setFirstName(Object firstName2) {
		firstName = (String) firstName2;
	}
	public void setLastName(Object lastName2) {
		lastName = (String) lastName2;
	}
}
