# market_api
Building RESTFul API Services using spring boot, MySQL and Swagger Documentation with containerization using Docker

Market API is a simple project to demonstrate a market place where customer meets merchants and the products.
Customer and merchant can register themselves through the app. Product categories can be retrieved as well.

Steps for executing :
1. Download the repository.
2. Open the project in the IDE and generate the executable .jar file for the application. The alternate method to generate the .jar file is through Maven.
3. Open docker-compose.yml file and add the MySQL environment parameter values and Spring REST API environment parameter values for database connection from the the application.
4. Run docker-compose up -d
5. After executing above steps without any errors and docker containers are up and running, open the browser and navigate to below url:
   http://localhost:9090/swagger-ui.html#/








